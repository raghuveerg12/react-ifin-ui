import React, { Component } from 'react';
import './burger.css';
import BurgerMenu from 'react-burger-menu';

class MenuWrap extends Component {
    constructor (props) {
      super(props);
      this.state = {
        hidden: false
      };
    }

    componentWillReceiveProps(nextProps) {
      const sideChanged = this.props.children.props.right !== nextProps.children.props.right;

      if (sideChanged) {
        this.setState({hidden : true});

        setTimeout(() => {
          this.show();
        }, this.props.wait);
      }
    }

    show() {
      this.setState({hidden : false});
    }

    render() {
      let style;

      if (this.state.hidden) {
        style = {display: 'none'};
      }

      return (
        <div style={style} className={this.props.side}>
          {this.props.children}
        </div>
      );
    }
  }


  class MenuLinks extends Component {
    constructor(props) {
        super(props);
  
        this.state = {
          currentMenu: 'slide',
          side: 'right'
        };
    }

    render() {
        const Menu = BurgerMenu[this.state.currentMenu];
        const items = [
            <a key="0" className="menu-item" href="/">Home</a>,
            <a key="1" className="menu-item" href="/help">Help</a>
        ];

        return (
            (this.state.side === 'right') ?
                  <MenuWrap wait={20} side={this.state.side}>
                    <Menu id={this.state.currentMenu} pageWrapId={'page-wrap'} outerContainerId={'outer-container'} right>
                      {items}
                    </Menu>
                  </MenuWrap>

              : <MenuWrap wait={20}>
                    <Menu id={this.state.currentMenu} pageWrapId={'page-wrap'} outerContainerId={'outer-container'}>
                      {items}
                    </Menu>
                  </MenuWrap>
        )
    }
  }

  export default MenuLinks;